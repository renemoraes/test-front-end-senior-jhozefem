const babel = require('gulp-babel');
const bower = require('gulp-bower');
const concat = require('gulp-concat');
const gulp = require('gulp');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');

// lista de htmls
const htmls = [
    './app/**/*.html'
];

// lista de imagens
const images = [
    './app/images/**/*.gif',
    './app/images/**/*.png',
    './app/images/**/*.jpg'
];

// lista de js
const scripts = [
    './app/src/scripts/init.js',
    './app/src/scripts/utils/init.js',
    './app/src/scripts/utils/*.js',
    './app/src/scripts/**/*.js'
];

// lista de estilos
const styles = [
    './app/src/styles/default.scss',
    './app/src/styles/**/*.scss'
];

// outros arquivos
const others = [
    './app/favicon.ico'
];

// nome do arquivo js gerado
const jsFileName = 'memed.js';

// nome do arquivo css gerado
const cssFileName = 'memed.css';

function showError(error) {
    console.log(error.toString());
    this.emit('end');
}

gulp.task('bower', function() {
    return bower();
});

gulp.task('htmls', function() {
    return gulp.src(htmls)
        .pipe(gulp.dest('./build'));
});

gulp.task('images', function() {
    gulp.src(images)
        .pipe(gulp.dest('./build/images'));
});

gulp.task('others', function() {
    return gulp.src(others)
        .pipe(gulp.dest('./build'));
});

gulp.task('scripts', ['bower'], function() {
    gulp.src(scripts)
        .pipe(concat(jsFileName))
        // ecma6 -> ecma5
        .pipe(babel({
            presets: [ 'es2015' ]
        }))
        // minifica o js
        .pipe(uglify({
            mangle: false,
            filename: jsFileName
        }))
        .on('error', showError)
        .pipe(gulp.dest('./build/js'));
});

gulp.task('styles', ['bower'], function() {
    return gulp.src(styles)
        .pipe(concat(cssFileName))
        // compila os arquivos scss
        .pipe(sass())
        .on('error', showError)
        .pipe(gulp.dest('./build/css'));
});

gulp.task('watch', function() {
    gulp.watch(htmls, [
        'htmls'
    ]);
    gulp.watch(images, [
        'images'
    ]);
    gulp.watch(others, [
        'others'
    ]);
    gulp.watch(scripts, [
        'scripts'
    ]);
    gulp.watch(styles, [
        'styles'
    ]);
});

gulp.task('build', [
    'htmls',
    'images',
    'others',
    'scripts',
    'styles'
]);

gulp.task('default', [
    'watch'
]);