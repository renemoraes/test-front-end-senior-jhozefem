const httpProxy = require('express-http-proxy');

// api-key usada nas requests
const apiKey = 'YvwM4rPAlxaxWaFKCffgLCZntOfTArsB4zabHzCntzEpQPLzM3jkPFNTtWjslYpN';

// secret-key usada nas requests
const secretKey = 'v9FQG00ruz25UF6NVfPVt7U664W62rNFRRemurKaqIvL56FRhFrVDEeSDmID4HlX';

/**
 * intermedia as requests da aplicação com o serviço de medicamentos
 */
module.exports = httpProxy('integracao.api.memed.com.br', {
	decorateRequest: function(proxyReq, originalReq) {
		proxyReq.headers['Accept'] = 'application/vnd.api+json';
		return proxyReq;
	},
	forwardPath: function(req, res) {
		return req.url + '&api-key=' + apiKey + '&secret-key=' + secretKey;
	},
	// transforma a resposta do serviço em uma nova usada na aplicação
	intercept: function(rsp, data, req, res, callback) {
		var response = [];
		var json = JSON.parse(data).data;

		for(var i = 0, len = json.length; i < len; i++) {
			var jsonItem = json[i];

			// formato enviado para a aplicação é { name, thumbnail }
			response.push({
				name: jsonItem.attributes.titulo,
				thumbnail: jsonItem.attributes.thumbnail
			});
		}

		callback(null, JSON.stringify(response));
	}
});