(function () {
	'use strict';

	angular
	    .module('memed-test')
	    .controller('indexController', indexController);

	indexController.$inject = [
		'$scope'
	];

	/**
	 * controller da páginal inicial da aplicação
	 */
	function indexController($scope) {
		// valor acessível na view
		$scope.acConfig = null;

		/**
		 * executado logo que o controller é iniciado
		 */
	    function _activate() {
	    	$scope.acConfig = {
	    		'input': '.index-page #search-input',
	    		'delay': 200
	    	};
	    }

	    _activate();
	}
})();