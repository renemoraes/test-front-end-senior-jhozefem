(function (win, doc) {
    'use strict';

    angular
        .module('memed-test')
        .controller('acWidgetController', acWidgetController);

    acWidgetController.$inject = [
        '$scope',
        '$timeout',
        '$element',
        '$window',
        'requestFactory'
    ];

    /**
     * controller para o componente de autocomplete
     */
    function acWidgetController($scope, $timeout, $element, $window, requestFactory) {
        /**
         * referência ao objeto do widget atual
         */
        var $this = this;

        /**
         * variável config que é setada no uso do componente
         */
        var _config;

        /**
         * objeto angular da window
         */
        var _win = null;

        /**
         * objeto angular do document
         */
        var _doc = null;

        /**
         * objeto angular do input text a qual o autocomplete está linkado
         */
        var _input = null;

        /**
         * indica se o hide() do widget é para ser feito
         */
        var _allowHide = true;

        /**
         * valor da última query feita request
         */
        var _lastQuery = null;

        /**
         * valor da última página feita request
         */
        var _lastPage = null;

        /**
         * ações estáticas do widget
         */
        const actions = [
            'Dose Habitual',
            'Prevenção de infecções por S. pneumoniae em indivíduos asplênicos',
            'Infecções do trato respiratório',
            'Infecções da pele e anexos',
            'Infecções do trato urinário'
        ];

        /**
         * tipos de paciente usados
         */
        const patientTypes = [
            'Adulto',
            'Pedriátrica',
            'Idosos'
        ];

        /**
         * formas físicas possíveis
         */
        const physicalForms = [
            'Cápsulas',
            'Comprimidos',
            'Cremes',
            'Gel'
        ];

        /**
         * frequências possíveis
         */
        const frequencies = [
            '2/2',
            '4/4',
            '8/8',
            '12/12',
            '24/24'
        ];

        /**
         * valores singular por tipo
         */
        const singular = {
            day: 'dia',
            hour: 'hora',
            kilo: 'kilo'
        };

        /**
         * valores plural por tipo
         */
        const plural = {
            day: 'dias',
            hour: 'horas',
            kilo: 'kilos'
        };

        /**
         * model usado no formulário para recuperar os dados da view
         */
        var formData = {
            patientType: patientTypes[0], // por default é o primeiro
            physicalForm: physicalForms[0], // por default é o primeiro
            patientWeight: 1,
            concentration: 1,
            frequency: 0,
            duration: 1
        };

        // valores acessíveis na view
        $this.actions = actions;
        $this.patientTypes = patientTypes;
        $this.physicalForms = physicalForms;
        $this.frequencies = frequencies;
        $this.formData = formData;
        $this.items = null;
        $this.widgetVisible = false;
        $this.isLoadingItems = false ;
        $this.isReqError = false;
        $this.calculate = calculate;
        $this.getNextItemsWidget = getNextItemsWidget;
        $this.selectPatientType = selectPatientType;
        $this.getPhysicalFormText = getPhysicalFormText;
        $this.getPatientWeightText = getPatientWeightText;
        $this.getConcentrationText = getConcentrationText;
        $this.getFrequencyText = getFrequencyText;
        $this.getDurationText = getDurationText;

        /**
         * obtém os medicamentos a partir da query para a página 1
         */
        function _getItemsWidget() {
            var query = this.value;
            var timer;

            // cancela o último timeout para evitar request desnecessária
            if(timer) {
                $timeout.cancel(timer);
            }

            // o delay é passado via config
            timer = $timeout(
                _getItemsRequest(query),
                $this._config.delay
            );
        }

        /**
         * obtém um texto em singular ou plural de acordo com o valor da quantidade
         */
        function _getTextByQty(value, singularText, pluralText) {
            var response = value;

            if(value === 1) {
                response += ' ' + singularText;
            }
            else {
                response += ' ' + pluralText;
            }

            return response;
        }

        /**
         * esconde o widget na view
         */
        function _hideWidget() {
            $this.widgetVisible = false;
            $scope.$apply();
        }

        /**
         * exibe o widget na view
         */
        function _showWidget() {
            $this.widgetVisible = true;
            $scope.$apply();

            // recalcula a posição x e y do widget na view
            _setWidgetPosition();
        }

        /**
         * realiza uma request para preencher os medicamentos
         */
        function _getItemsRequest(query, page) {
            // se não for passado a página, o valor é 1
            if(typeof page === 'undefined') {
                page = 1;
            }

            // trim() na query
            if(typeof query !== 'undefined') {
                query = query.trim();
            }

            // últimos valores para query e page
            _lastQuery = query;
            _lastPage = page;

            $this.isReqError = false;

            // não foi passado a query, não exibe os container de medicamentos no widget
            if(!query) {
                $this.items = null;
                $this.isLoadingItems = false;
            }
            else {
                $this.isLoadingItems = true;

                // faz a request usando a factory
                requestFactory.getItems(query, page,
                    function(resp) {
                        if(resp.error === false) {
                            // quando a página for maior que 1, concatena os itens já existente com o novo
                            if(page > 1) {
                                $this.items = [ ...$this.items, ...resp.data ];
                            }
                            else {
                                $this.items = resp.data;
                            }
                        }
                        else {
                            $this.isReqError = true;
                        }

                        $this.isLoadingItems = false;
                    });
            }
        }

        /**
         * calcula a posição x e y do widget na view
         */
        function _setWidgetPosition() {
            var offset = $this._input[0].getBoundingClientRect();

            // a posição y é 5px abaixo do input
            $element.css('top', (
                offset.top +
                $this._input.prop('offsetHeight') +
                5
            ) + 'px');

            // a posição y indica que o widget deve ficar ao centro, com base no input
            $element.css('left', (
                offset.left +
                $this._input.prop('offsetWidth') / 2 -
                $element.prop('offsetWidth') / 2
            ) + 'px');
        }

        /**
         * liga listeners nos elementos
         */
        function _bindElements() {
            // ao clicar na página, hide() no widget
            $this._doc.on('click', function() {
                // só esconde se estiver habilitado
                if(_allowHide === true) {
                    _hideWidget();
                }

                _allowHide = true;
            });

            // clique no elemento, indica que o hide() do document.click() não é para ser feito
            $element.on('click', function() {
                _allowHide = false;
            });

            // focus no input, exibe o widget
            $this._input.on('focus', _showWidget);

            // clique no input, exibe o widget
            $this._input.on('click', function() {
                // indica que o hide() do document.click() não é para ser feito
                _allowHide = false;

                _showWidget();
            });

            // keyup no input, realiza consulta na api
            $this._input.on('keyup', _getItemsWidget);

            // ao dar resize na window, recalcula o x e y novamente
            $this._win.on('resize', _setWidgetPosition);
        }

        /**
         * parse do config setado para o componente
         */
        function _parseConfig() {
            $this._win = angular.element(win);
            $this._doc = angular.element(doc);
            $this._input = angular.element(doc.querySelector($this._config.input));

            _bindElements();

            // calcula a posição x e y do elemento
            $timeout(_setWidgetPosition, 0);
        }

        /**
         * ouve as alterações no atributo config setado no uso do componente
         */
        function _configWatch() {
            $scope.$watch(function () {
                return $this.config;
            }, function (value) {
                if (typeof value !== 'undefined') {
                    $this._config = value;

                    // depois de setado o novo valor do config, faz o parse novamente
                    _parseConfig();
                }
            });
        }

        /**
         * método chamado no formulário para concluir a posologia
         */
        function calculate() {
            $window.alert('O cálcula da dosagem adequada para um paciente de ' +
                getPatientWeightText() + ' é de: 1 ' +
                getPhysicalFormText() + ' de ' +
                getConcentrationText() + ' de ' +
                getFrequencyText() + ' por ' +
                getDurationText() + '.');
        }

        /**
         * obtém os medicamentos para a próxima página
         */
        function getNextItemsWidget() {
            _getItemsRequest(_lastQuery, ++_lastPage);
        }

        /**
         * seleção de um novo tipo de paciente no formulário
         */
        function selectPatientType(patientType) {
            $this.formData.patientType = patientType;
        }

        /**
         * obtenção de texto para a forma física
         */
        function getPhysicalFormText() {
            return $this.formData.physicalForm;
        }

        /**
         * obtenção de texto para o peso do paciente
         */
        function getPatientWeightText() {
            var patientWeight = $this.formData.patientWeight;

            // utiliza o método para entender se é o texto de singular ou plural
            return _getTextByQty(
                patientWeight,
                singular.kilo,
                plural.kilo
            );
        }

        /**
         * obtenção de texto para concentração
         */
        function getConcentrationText() {
            return $this.formData.concentration;
        }

        /**
         * obtenção de texto para frequência
         */
        function getFrequencyText() {
            var frequency = $this.formData.frequency;

            return frequencies[frequency] + ' ' + plural.hour;
        }

        /**
         * obtenção de texto para duração
         */
        function getDurationText() {
            var duration = $this.formData.duration;

            // utiliza o método para entender se é o texto de singular ou plural
            return _getTextByQty(
                duration,
                singular.day,
                plural.day
            );
        }

        /**
         * executado logo que o controller é iniciado
         */
        function _activate() {
            // inicializa o listener do config
            _configWatch();
        }

        _activate();
    }
})(window, document);