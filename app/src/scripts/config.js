(function () {
	'use strict';

	angular
		.module('memed-test')
		.config(config);

	config.$inject = [
		'$stateProvider',
		'$urlRouterProvider',
		'CacheFactoryProvider'
	];

	/**
	 * rotas da aplicação
	 */
	function config($stateProvider, $urlRouterProvider, CacheFactoryProvider) {
		$stateProvider.state('app', {
			abstract: true,
			views: {
				'header': {
					templateUrl: 'views/header.html'
				},
				'footer': {
					templateUrl: 'views/footer.html'
				}
			}
		})
		.state('app.index', {
			url: '/',
			views: {
				'main@': {
					templateUrl: 'views/index/main.html'
				}
			}
		});

		// cache guardado por no máximo 1 hora
		angular.extend(CacheFactoryProvider.defaults, {
			maxAge: 60 * 1000
		});

		$urlRouterProvider.otherwise('/');
	}
})();