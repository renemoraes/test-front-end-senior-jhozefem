(function () {
	'use strict';

	/**
	 * configurações para o componente de autocomplete
	 */
	var _acWidgetComponent = {
		templateUrl: 'views/components/ac-widget.html',
		controller: 'acWidgetController',
		bindings: {
			config: '<'
		}
	};

	angular
	    .module('memed-test')
	    .component('acWidget', _acWidgetComponent);
})();