angular
    .module('memed-test')
    .factory('requestFactory', requestFactory);

requestFactory.$inject = [
	'$http',
	'CacheFactory'
];

function requestFactory($http, CacheFactory) {
	/**
	 * quantidade de medicamentos por página
	 */
	const _itemsPerPage = 5;

	/**
	 * índice usado pelo angular-cache para cachear as consultas
	 */
	const _cacheName = 'acWidgetReqCache';

	/**
	 * id da última requisição
	 */
	var _lastReqId = null;

	/**
	 * gera um id a ser usado na request
	 */
	function _generateRequestId() {
		return (Math.random() + '').replace(/[^0-9]/g, '');
	}

	/**
	 * realiza uma request a partir dos parâmetros
	 */
	function _makeRequest(params, callback) {
		var reqId;
		var cache;

		// se o cache não existir ainda, cria
		if (!(cache = CacheFactory.get(_cacheName))) {
            cache = CacheFactory.createCache(_cacheName, {
                deleteOnExpire: 'aggressive'
            });
        }

        // obtém o id gerado para a nova request
        reqId = _generateRequestId();

        // faz a request para o proxy
		$http({
			method: 'GET',
			url: '/proxy/v1/apresentacoes',
			cache: cache,
			params: params
		})
		.then(function(response) {
			callback(reqId, {
				error: false,
				data: response.data
			});
		},
		function() {
			callback(reqId, {
				error: true
			});
		});

		return reqId;
	}

	/**
	 * obtém os medicamentos a partir da query e page
	 */
	function getItems(query, page, callback) {
		_lastReqId = _makeRequest({
			'filter[categoria]': 'industrializados',
			'filter[q]': query,
			'page[limit]': _itemsPerPage,
			'page[offset]': (page - 1) * _itemsPerPage
		}, function(reqId, resp) {
			if(_lastReqId === reqId) {
				callback(resp);
			}
		});
	}

	return {
		getItems
	}
}