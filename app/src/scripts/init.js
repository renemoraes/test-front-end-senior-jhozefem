(function () {
	'use strict';

	angular
		.module('memed-test', [
			'angular-cache',
			'ui.router'
		]);
})();