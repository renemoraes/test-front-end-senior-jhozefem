const express = require('express');
const proxyInterceptor = require('./lib/interceptors/proxy-interceptor');

const app = express();
const port = process.env.PORT || 8080;

app.use(express.static('build'));

app.use('/proxy', proxyInterceptor);

app.listen(port, function() {
    console.log('listen to port ' + port);
});