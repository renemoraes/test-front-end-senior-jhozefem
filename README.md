# Memed - Teste para Front-End Senior (Angular JS)

## Pré-requisitos

* [npm](https://www.npmjs.com/)
* [Gulp](http://gulpjs.com/)

## Proxy API

Embora o foco do teste seja o Front-End, criei um proxy API pensando em escalabilidade. Para a aplicação não depender da API, criei o proxy para intermediar as requests. Com ele dá pra subir cache, centralizar o acesso aos endpoints, etc. O proxy API foi desenvolvido em Node usando o Express.

Além disso, como a minha aplicação só depende de imagem e nome do remédio, o JSON de resposta foi modificado para apenas retornar esses 2 valores.

## Start do projeto

Depois que os pré-requisitos existirem na máquina, para dar start no projeto basta acessar a pasta do mesmo e executar o comando abaixo:

```console
$ npm install ; npm run dev
```

O comando vai dar start no front-end e no back-end. Os 2 vão subir na porta 8080, o Express controla a subida dos 2. Para visualizar o conteúdo no browser, basta abrir a url http://localhost:8080/.

## Funcionalidades

### Exibição do Autocomplete

O componente aparece assim que o input tem focus e some assim que é clicado fora do componente.

### Posicionamento do componente

Há um cálculo automático do posicionamento do componente na view. Ele fica centralizado ao input e abaixo do mesmo em 5px. Se for dado um resize na página, o mesmo será reposicionado.

### Obtenção de medicamentos

Quando há uma consulta vazia no input, o container de medicamentos fica invisível. Sempre que é digitado algo no input, são realizadas consultas na API e renderizada a resposta no componente.

### Delay para evitar requests desnecessárias

No config do componente há um campo chamado "delay". Esse valor é em ms e indica que se o usuário digitar um caractere no input e digitar em seguida outro em menos de 200ms, a consulta anterior não é feita, apenas a em seguida. Esse valor pode ser modificado, está parametrizado.

### Garantia que última request será renderizada

As requests para a API são assíncronas, logo tem um grande perigo de que se o usuário digitar "ceto" e logo em seguida digitar mais um caractere "c", a resposta de "cetoc" responder primeiro e só depois "ceto" ser respondida. Se não houver controle, o caso anterior irá renderizar "ceto", mesmo que tenha no input "cetoc".

Para resolver isso, foi criado um id por request, onde é guardado sempre o id da última request feita e garantia na reposta de que aquela é a ser renderizada.

### Paginação de medicamentos

A paginação de medicamentos é feita no clique do link "Mais..." abaixo da resposta dos medicamentos. No seu clique, é feita uma nova consulta na API, mas modificando o offset.

### Cache de consultas

Todas as consultas são cacheadas em memória, ou seja, se o usuário digitar "ceto", digitar outra coisa e novamente "ceto", não será feito novamente uma consulta a API, visto que o JSON já está guardado. Por ser em memória, se o usuário der F5 o cache é limpo, logo a consulta "ceto" irá gerar uma request.

Foi definido que o cache por padrão vive por 1 hora. Se a consulta passar disso no cache, ela é apagada e na inserção de seu valor no input, a aplicação irá fazer uma nova consulta na API.

### Formulário de posologia

O formulário é exibido no clique dos itens estáticos do widget. Lá é gerado um alert() no clique do botão, e as informações do alert são de acordo com o formulário.

## Informações adicionais

### Gif de loading

Como eu não tinha o gif de loading que tem no vídeo, usei um que eu tinha no meu computador.

### Input de range

Infelizmente não consegui fazer com que o input tivesse um azul no range selecionado. Sendo assim, acabei deixando tudo cinza.